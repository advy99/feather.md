# 1.3.0

- Keep in memory last interacted with folder
- Added a way to reopen recents files in app
- Switched to MeditorJS 1.0.1
- Improved tabs design
- Added close tab button/save status
- Display file path in the application title
- Tabs have a separated undo/redo history
- Several bug fixes for MacOS

# 1.2.0

* Handle opening of multiple files at once via the explorer
* Hide dev tools in production

# 1.1.0

* Ask to save unsaved files before exiting
* Translated dialogs to english
* Open links outside the application
* Fixed tab's title generation

# 1.0.0

Initial release