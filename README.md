# Feather.md

Feather.md is a Windows/Linux/MacOS application to edit markdown files with ease.
It uses [MeditorJS](https://gitlab.com/cobalt-os-team/MeditorJS) to render the markdown.

![preview](https://gitlab.com/cobalt-os-team/feather.md/-/raw/dev/build/interface.png)
## How to use

You can download the installer from the [releases](https://gitlab.com/cobalt-os-team/feather.md/-/releases). Note that if you are on MacOS you will have to compile the application on your own.
